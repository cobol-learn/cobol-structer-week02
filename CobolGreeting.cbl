       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CobolGreeting.
      *display cobol ggreetings program
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 ITERNUM PIC 9 VALUE 5.

       PROCEDURE DIVISION.
       BeginProgram.
           PERFORM DISPLAYGREETING ITERNUM TIMES.
           STOP RUN.
       
       DISPLAYGREETING.
           DISPLAY "GREETING FROM COBOL.".