       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONDITION-NAME.
       AUTHOR. PAKAWAT JAROENYING.

      *LEVEL 88 is   condition name
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  CharIN   PIC   X  .
        88 VoWel VALUE "a","e","i","o","u".
        88 Consonant   VALUE "b" , "c","d","f","g","h",
                             "j" THRU "n", "p" THRU "t",
                             "v" THRU "z".
        88 Digit VALUE "0" THRU "9".
        88 ValidCharacter VALUE "a" THRU "z" , "0" THRU "9".

       PROCEDURE DIVISION .
      *Begin is paragraph
       Begin.
           DISPLAY 
              "Enter lower case character or digit. Invalid char end". 
      *       end sentence
           ACCEPT CharIN 
           PERFORM UNTIL NOT ValidCharacter 
              EVALUATE TRUE
                 WHEN VoWel DISPLAY 
                                   "The letter " CharIN " is a vowel."
                 WHEN Consonant  DISPLAY 
                                 "The letter " CharIN " is a consonant."
                 WHEN Digit DISPLAY
                             CharIN " is digit."                
              END-EVALUATE
              ACCEPT CharIN 
           END-PERFORM
           STOP RUN.
      *    end sentence